$('.carousel').carousel({
    interval: 1500
  })
  $(function () {
    $('[data-toggle="popover"]').popover()
  })
  $('#myModal').on('show.bs.modal', function(e) {
    console.log('el modal se empezo a mostrar');
  });
  $('#myModal').on('shown.bs.modal', function(e) {
    console.log('el modal se esta mostrando');
    $('#botonModal').removeClass('btn-primary');
    $('#botonModal').addClass('btn-secondary');
    $('#botonModal').prop('disabled',true);
  });
      
        
      
  $('#myModal').on('hide.bs.modal', function(e) {
    console.log('el modal se empezo a dear de mostrar');
  });
  $('#myModal').on('hidden.bs.modal', function(e) {
    console.log('el modal se dejo de mostrar');
    $('#botonModal').removeClass('btn-secondary');
    $('#botonModal').addClass('btn-primary');
    $('#botonModal').prop('disabled',false);
  });